/**
 * @file    tree_mesh_builder.cpp
 *
 * @author  Matěj Kudera <xkuder04@stud.fit.vutbr.cz>
 *
 * @brief   Parallel Marching Cubes implementation using OpenMP tasks + octree early elimination
 *
 * @date    DATE
 **/

#include <iostream>
#include <math.h>
#include <limits>

#include "tree_mesh_builder.h"

TreeMeshBuilder::TreeMeshBuilder(unsigned gridEdgeSize)
    : BaseMeshBuilder(gridEdgeSize, "Octree")
{

}

unsigned TreeMeshBuilder::marchCubes(const ParametricScalarField &field)
{
    // Suggested approach to tackle this problem is to add new method to
    // this class. This method will call itself to process the children.
    // It is also strongly suggested to first implement Octree as sequential
    // code and only when that works add OpenMP tasks to achieve parallelism.

    unsigned totalTriangles = 0;

    // Make 8 children from grid
    Vec3_t<float> cube_pos_in_grid(0, 0, 0);
    #pragma omp parallel
    {
        #pragma omp master
        {
            totalTriangles = makeSubCubes(mGridSize, cube_pos_in_grid, field);
        }
    }
    
    return totalTriangles;
}

float TreeMeshBuilder::evaluateFieldAt(const Vec3_t<float> &pos, const ParametricScalarField &field)
{
    // NOTE: This method is called from "buildCube(...)"!

    // 1. Store pointer to and number of 3D points in the field
    //    (to avoid "data()" and "size()" call in the loop).
    const Vec3_t<float> *pPoints = field.getPoints().data();
    const unsigned count = unsigned(field.getPoints().size());

    float value = std::numeric_limits<float>::max();

    // 2. Find minimum square distance from points "pos" to any point in the
    //    field.
    for(unsigned i = 0; i < count; ++i)
    {
        float distanceSquared  = (pos.x - pPoints[i].x) * (pos.x - pPoints[i].x);
        distanceSquared       += (pos.y - pPoints[i].y) * (pos.y - pPoints[i].y);
        distanceSquared       += (pos.z - pPoints[i].z) * (pos.z - pPoints[i].z);

        // Comparing squares instead of real distance to avoid unnecessary
        // "sqrt"s in the loop.
        value = std::min(value, distanceSquared);
    }

    // 3. Finally take square root of the minimal square distance to get the real distance
    return sqrt(value);
}

void TreeMeshBuilder::emitTriangle(const BaseMeshBuilder::Triangle_t &triangle)
{
    // NOTE: This method is called from "buildCube(...)"!

    // Store generated triangle into vector (array) of generated triangles.
    // The pointer to data in this array is return by "getTrianglesArray(...)" call
    // after "marchCubes(...)" call ends.
    #pragma omp critical
    mTriangles.push_back(triangle);
}

unsigned TreeMeshBuilder::makeSubCubes(const unsigned cubes_on_edge, const Vec3_t<float> parent_cube_pos_in_grid, const ParametricScalarField &field)
{
    unsigned triangles_in_tree = 0;

    // Recursion cut-off
    if (cubes_on_edge > 1)
    {
        // Each off 3 dimensions devided by 2
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                for (int k = 0; k < 2; k++)
                {
                    // Calculate start position off cube
                    unsigned x_grid = parent_cube_pos_in_grid.x + cubes_on_edge/2*i;
                    unsigned y_grid = parent_cube_pos_in_grid.y + cubes_on_edge/2*j;
                    unsigned z_grid = parent_cube_pos_in_grid.z + cubes_on_edge/2*k;
                    Vec3_t<float> cube_pos_grid(x_grid, y_grid, z_grid);

                    // Get middle position of cube in physical coordinates
                    float x_mid_real = (x_grid+cubes_on_edge/4.0) * mGridResolution;
                    float y_mid_real = (y_grid+cubes_on_edge/4.0) * mGridResolution;
                    float z_mid_real = (z_grid+cubes_on_edge/4.0) * mGridResolution;
                    Vec3_t<float> cube_middle_point(x_mid_real, y_mid_real, z_mid_real);

                    // Check if surface is possible in this cube
                    float check_value = field.getIsoLevel() + (sqrt(3.0)/2.0)*(cubes_on_edge/2*mGridResolution);
                    
                    if (evaluateFieldAt(cube_middle_point, field) <= check_value)
                    {
                        // Make new cubes
                        #pragma omp task shared(triangles_in_tree)
                        triangles_in_tree += makeSubCubes(cubes_on_edge/2, cube_pos_grid, field);
                    }
                }
            }
        }
    }
    else
    {
        // Make cube on lowest level
        unsigned triangles_in_cube = buildCube(parent_cube_pos_in_grid, field);
        #pragma omp atomic write
        triangles_in_tree = triangles_in_cube;
    }

    #pragma omp taskwait
    return triangles_in_tree;
}
